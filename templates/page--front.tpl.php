<div id="page" class="row <?php print $classes; ?>"<?php print $attributes; ?>>

  <!--  HEADER  -->

  <header id="header" class="row">
    <div class="container large-12 columns">

      <?php if ($site_name || $site_slogan): ?>
        <div id="name-and-slogan">

          <?php if ($site_name): ?>
            <?php if ($title): ?>
              <div id="site-name">
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a>
              </div>
            <?php else: /* Use h1 when the content title is empty */ ?>
              <h1 id="site-name">
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a>
              </h1>
            <?php endif; ?>
          <?php endif; ?>

          <?php if ($site_slogan): ?>
            <div id="site-slogan"><?php print $site_slogan; ?></div>
          <?php endif; ?>

        </div>
      <?php endif; ?>

    </div>
  </header> <!-- /header -->

  <!--  MAIN  -->

  <div id="main" class="row">
    <div class="container">

      <?php if ($page['sidebar_first']): ?>
        <aside id="sidebar-first" class="large-4 medium-5 large-push-8 medium-push-7 columns sidebar first">
          <?php print render($page['sidebar_first']); ?>
        </aside>
      <?php endif; ?> <!-- /sidebar-first -->

      <?php if ($user->uid): ?>
        <?php if ($page['sidebar_second']): ?>
          <aside id="sidebar-second" class="large-4 large-push-8 medium-5 medium-push-7 columns sidebar second">
            <?php print render($page['sidebar_second']); ?>
          </aside>
        <?php endif; ?> <!-- /sidebar-second -->
      <?php endif; ?>

      <section id="content" class="large-8 large-pull-4 medium-7 medium-pull-5 columns">

        <?php print $messages; ?>

        <h2>Fresno County's Single Sign-on Solution for Online Educational Resources</h2>

        <p>The FCOE Portal simplifies access to instructional resources and digital tools by eliminating the need for multiple usernames and passwords. Teachers and students log in to the Portal and then click right on through to their provided resources.</p>

        <ul>
          <li>Single-site login to curriculum resources for your district's teachers and students</li>
          <li>Ease the burden of multiple user names and passwords for different websites</li>
          <li>Great pricing for Discovery Education, Follett Destiny, TumbleBook Library and more</li>
          <li><a href="/subscription-plans">Subscription plans</a> to suit the needs of your district</li>
        </ul>

        <p>To get the Portal for your district, contact us at (559) 497-3711.</p>
        <p>Also, you can visit <a href="http://its.fcoe.org/">Instructional Technology Services</a> for a list of our workshops and services.</p>

      </section> <!-- /content-inner /content -->

    </div>
  </div> <!-- /main -->

  
<!--
  <div class="row partner-logos">
    <h4><span>Our Partners</span></h4>
    <div class="row">
      <img src="/sites/all/themes/fcoeportal/images/partner_logos.jpg" />
    </div>
  </div>
--> 

<div class="row resources">
  <div class="columns large-6 resource-details">
    <div class="inner first">
      <h4>Our Resources</h4>
      <p class="text-left">Our Standard Subscription Plans provide access to the following resources.</p>
      <div class="resource-images">
        <a href="#"><img src="/sites/all/themes/fcoeportal/images/img_logo_follett.png" alt="teachingbooks.net"></a>
        <a href="#"><img src="/sites/all/themes/fcoeportal/images/img_logo_tumble.png" alt="teachingbooks.net"></a>
        <a href="#"><img src="/sites/all/themes/fcoeportal/images/img_logo_discovery.png" alt="teachingbooks.net"></a>
      </div>
      <p><a href="/subscription-plans" class="button orange">Get the Portal for Your District</a></p>
    </div>
  </div>
  <div class="columns large-6 resource-details">
    <div class="inner last">
      <h4>California k-12 Resources</h4>
      <p class="text-left">The State of California provides free access to the following curriculum resources for qualifying schools.</p>
      <div class="resource-images">
        <a href="#"><img src="/sites/all/themes/fcoeportal/images/img_logo_tb.png" alt="teachingbooks.net"></a>
        <a href="#"><img src="/sites/all/themes/fcoeportal/images/img_logo_proquest.png" alt="teachingbooks.net"></a>
        <a href="#"><img src="/sites/all/themes/fcoeportal/images/img_logo_britannica.png" alt="teachingbooks.net"></a>
      </div>
      <p><a href="/california-resources" class="button orange">Browse California k-12 Resources</a></p>
    </div>
  </div>
</div>


<style type="text/css">

.resource-details {
  margin-top: 30px;
  }

.resource-details .inner {
  background: #f7f7f7;
  text-align: center;
  padding: 20px 30px;
  }

.resource-details h4 {
  font-weight: bold;
  text-transform: uppercase;
  font-size: 28px;
  color: #053e6f;
  }

.resource-details p.tex-left {
  text-align: left;
  }

.resource-details .inner.first {
  margin-right: 20px;
  }

.resource-details .inner.last {
  margin-left: 20px;
  }

.resource-details a {
  display: block;
  text-align: center;
  margin: 0 auto;
  margin-bottom: 14px;
}

.resource-details .inner.first a {
  margin-bottom: 24px;
}

.resource-details a img {
  display: block;
  margin: 0 auto;
  
  }

.resource-details p a {
    border: 0;
    border-style: solid;
    border-width: 0;
    cursor: pointer;
    font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Roboto,Arial,sans-serif;
    font-weight: normal;
    line-height: normal;
    margin: 20px 0 1.25rem;
    position: relative;
    text-decoration: none;
    text-align: center;
    -webkit-appearance: none;
    -moz-appearance: none;
    border-radius: 0;
    display: inline-block;
    padding-top: 0.4375rem;
    padding-right: 0.875rem;
    padding-bottom: 0.5rem;
    padding-left: 0.875rem;
    background-color: #008CBA;
    border-color: #007095;
    color: #FFFFFF;
    transition: background-color 300ms ease-out;
    font-size: 1.125rem;
    text-transform: uppercase;
    background: #fbb033;
    margin-bottom: 0.5rem !important;
}
</style>

  <?php if ($page['footer']): ?>
    <footer id="footer" class="row">
      <div class="container large-12 columns">
      <?php print render($page['footer']); ?>
      </div>
    </footer> <!-- /footer -->
  <?php endif; ?>

</div> <!-- /page -->
