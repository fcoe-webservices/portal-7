<div id="page" class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <!--  HEADER  -->

  <header id="header" class="row">
    <div class="container large-12 columns">
      <?php print render($page['sidebar_second']); ?>
    </div>
  </header> <!-- /header -->



  <!--  MAIN  -->

  <div id="main" class="row">
    <div class="container">
      <section id="content" class="large-12 columns">
  
          <?php if ($title|| $messages || $tabs || $action_links): ?>
            <div id="content-header">

              <?php if ($page['highlighted']): ?>
                <div id="highlighted"><?php print render($page['highlighted']) ?></div>
              <?php endif; ?>

              <?php print render($title_prefix); ?>

              <?php if ($title): ?>
                <h1 class="title"><?php print $title; ?></h1>
              <?php endif; ?>

              <?php print render($title_suffix); ?>
              <?php print $messages; ?>
              <?php print render($page['help']); ?>

              <?php if ($tabs): ?>
                <div class="tabs"><?php print render($tabs); ?></div>
              <?php endif; ?>

              <?php if ($action_links): ?>
                <ul class="action-links"><?php print render($action_links); ?></ul>
              <?php endif; ?>

            </div> <!-- /#content-header -->
          <?php endif; ?>

          <div id="content-area">
            <?php print render($page['content']) ?>
          </div>

      </section> <!-- /content-inner /content -->

    </div>
  </div> <!-- /main -->

  <!--  FOOTER  -->

  <?php if ($page['footer']): ?>
    <footer id="footer" class="row">
      <div class="container large-12 columns">
      <?php print render($page['footer']); ?>
      </div>
    </footer> <!-- /footer -->
  <?php endif; ?>

</div> <!-- /page -->
