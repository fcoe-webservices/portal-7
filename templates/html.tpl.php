<!DOCTYPE html>
<html>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <?php print $head_scripts; ?>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
</head>
<body class="<?php print $classes; ?>" <?php print $attributes; ?>>
  <div id="skip">
    <a href="#content"><?php print t('Jump to Navigation'); ?></a>
  </div>

  <!-- fcoe banner -->
  <div id="fcoe-banner-no-cols">
    <div class="row">
      <div class="large-12 small-12 columns">
        <div class="fcoe-logo"><a href="http://www.fcoe.org"></a></div>
        <div class="fcoe-title"><h2>Fresno County Superintendent of Schools <span>Dr. Michele Cantwell-Copher, Superintendent</span></h2></div>
      </div>
    </div>
  </div>

  <?php print $page_top; ?>
  <?php print $page; ?>
  <div class="footer-final style="clear:both">
  &nbsp;
  </div>
  <?php print $scripts; ?>
  <?php print $page_bottom; ?>

</body>
</html>