
<div id="page" class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <!--  HEADER  -->

  <header id="header" class="row">
    <div class="container large-12 columns">
      <?php print render($page['sidebar_second']); ?>
    </div>
  </header> <!-- /header -->



  <!--  MAIN  -->

  <div id="main" class="row">
    <div class="container">
      <section id="content" class="large-12 columns">

          <?php if ($title|| $messages || $tabs || $action_links): ?>
            <div id="content-header">

              <?php if ($page['highlighted']): ?>
                <div id="highlighted"><?php print render($page['highlighted']) ?></div>
              <?php endif; ?>

              <?php print render($title_prefix); ?>

              <?php if ($title): ?>
                <h1 class="title"><?php print $title; ?></h1>
              <?php endif; ?>

              <?php print render($title_suffix); ?>
              <?php print $messages; ?>
              <?php print render($page['help']); ?>

              <?php if ($tabs): ?>
                <div class="tabs"><?php print render($tabs); ?></div>
              <?php endif; ?>

              <?php if ($action_links): ?>
                <ul class="action-links"><?php print render($action_links); ?></ul>
              <?php endif; ?>

            </div> <!-- /#content-header -->
          <?php endif; ?>

          <ul id="content-area " class="small-block-grid-2">
            <?php
            $uemail = $user->mail;
            $resource_ids = array();
            $result = db_query('SELECT *
              FROM {mresource_resmems} WHERE user = :uemail', array(':uemail' => $uemail));

            $ml_ids =  $result->fetchCol(0);
            $mls = node_load_multiple($ml_ids);

            foreach($mls as $ml) {
              $resource_ids[] = $ml->field_resource['und'][0]['target_id'];
            }

            $resources = node_load_multiple($resource_ids);
            $listings = array();
            $i = 0;
            foreach($resources as $resource_item){
              $listings[$i]['ml_id'] = $ml_ids[$i];
              $listings[$i]['resource'] = $resource_item;
              $i++;
            }


            foreach($listings as $listing){
                $field_resource_image = field_get_items('node', $listing['resource'], 'field_resource_image');
                $path = file_create_url($listing['resource']->field_resource_image[und][0][uri]);
              ?>
              <li class="portal-resource">
                <div class="columns large-3 medium-3">
                  <a target="_blank" href="/mresource/login/<?= $listing['ml_id']; ?>"><img typeof="foaf:Image" src="<?php echo $path; ?>" alt=""></a>
                </div>
                <div class="columns large-9 medium-9">
                  <a target="_blank" href="/mresource/login/<?= $listing['ml_id']; ?>"><h4><?= $listing['resource']->title; ?></h4></a>
                  <p><?= $listing['resource']->body['und'][0]['value']; ?></p>
                  <?php $listingtitle = str_ireplace('fresno county superintendent of schools', 'FCSS', $listing['resource']->title); ?>
                  <a href="/mresource/login/<?= $listing['ml_id']; ?>" target="_blank" class="button">Login to <?= $listingtitle ?></a>
                </div>
              </li>
              <?php
            }


            ?>
            <?php //print render($page['content']) ?>
          </ul>

      </section> <!-- /content-inner /content -->

    </div>
  </div> <!-- /main -->

  <!--  FOOTER  -->

  <?php if ($page['footer']): ?>
    <footer id="footer" class="row">
      <div class="container large-12 columns">
      <?php print render($page['footer']); ?>
      </div>
    </footer> <!-- /footer -->
  <?php endif; ?>

</div> <!-- /page -->
