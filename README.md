# FCOE Drupal Skeleton Theme (Whiplash)

This is the default sekeleton theme for FCOE Drupal sites. The theme is meant as a skeleton

## Getting started

Whiplash is built with Foundation 5. To run it, you'll need the following

  * [Node.js](http://nodejs.org): Use the installer provided on the NodeJS website.
  * [Grunt](http://gruntjs.com/): Run `[sudo] npm install -g grunt-cli`
  * [Bower](http://bower.io): Run `[sudo] npm install -g bower`

## Quickstart

```bash
git clone https://USERNAME@bitbucket.org/fcoe-webservices/whiplash.git
npm install && bower install
```

  1. Clone the theme using `mv whiplash NEW_THEME_NAME`
  2. Rename the .info file (change references in the template.php file also)
  3. Uncomment the Foundation elements needed in `scss/app.scss`
  4. Reconfigure the settings in the `scss/_settings.scss` file
  5. Remove the original .git file
  6. Initialize git and set up a new repo
  7. Remove the js and css folders from the `.gitignore` file

While you're working on your project, run: `grunt` to build css and js files.

## Directory Structure

  * `scss/_settings.scss`: Foundation configuration settings go in here
  * `scss/app.scss`: Application styles go here
  * `templates/` : All Drupal tpl files go here
  * `js/` : All js files go here
  * `bower_components` : All bower packages are loaded here
  * `custom_components` : all non-bower packages are placed here

## Drupal Configuration

Foundation 5 requires Jquery 1.10+ to work. Activate via the [jQuery Update Module](https://www.drupal.org/project/jquery_update).

###Scripts
Javascript is aggregated into two files: `production.top.js` called by the `$head_scripts` variable, and `production.bottom.js`, called by the `$scripts` variable. The first holds all javascript to be called in the head, the second is called before the `body` tag closes.

You must update references to the THEMENAME in both the .info and the template.php file.

## SASS Configuration

###To-do

  * Add `_mixins.scss` or similar
  * Reconfigure `_settings.scss` to play nice with paths
  * Configure for fonts

## Adding Javascript/CSS libraries

Library files are located in the `bower_components` or `custom_components` folder. Use custom components for any libraries that do not have bower packages.

Follow these steps to add a javascript file.

  1. Install via bower, or download and place in the `custom_components` folder
  2. Add the path to the file in the `top/;` or `bottom/;` array inside `Gruntfile.js`.
  3. You're done! Only put javascript that needs to be loaded in the `<head>` within the `top:` array.