<?php

/* Implements https://gist.github.com/pascalduez/1418121 */

/**
 * Implements hook_preprocess_html().
 */
function fcoeportal_preprocess_html(&$vars) {
  // Move JS files "$scripts" to page bottom for perfs/logic.
  // Add JS files that *needs* to be loaded in the head in a new "$head_scripts" scope.
  // Reference the Gruntfile for a list of js scripts to be bundled.
  $path = drupal_get_path('theme', 'fcoeportal');
  drupal_add_js($path . '/js/production.top.js', array('scope' => 'head_scripts', 'weight' => -1, 'preprocess' => FALSE));
}

/**
 * Implements hook_process_html().
 */
function fcoeportal_process_html(&$vars) {
  $vars['head_scripts'] = drupal_get_js('head_scripts');
}

function fcoeportal_form_alter(&$form, &$form_state, &$form_id){
  if($form_id == 'user_login_block') {
    //print_r($form);
    $form['links']['#weight']  = 100;
  }
}

function fcoeportal_preprocess_page(&$vars) {
  $alias_parts = explode('/', drupal_get_path_alias());
  if (count($alias_parts) && $alias_parts[0] == 'my-resources') {
    $vars['theme_hook_suggestions'][] = 'page__myresources';
  }
}
